# Architecture Hexagonale TDD

## Installation
Installation des dépendances avec `npm install`

## Lancement de l'application
Choisir les Adapter à utiliser : variables inputConsole et outputConsole dans le `main.ts`

Lancement de l'application : `npm start`

## Lancement des tests
Lancement des tests avec `npm test`