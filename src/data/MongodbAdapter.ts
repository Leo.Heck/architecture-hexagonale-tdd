import ISaveAndLoad from '../game/ISaveAndLoad';

let MongoClient = require('mongodb').MongoClient;

export class MongodbAdapter implements ISaveAndLoad {
    url: string;
    databaseName: string;
    collectionName: string;

    constructor({databaseUrl, databaseName, collectionName}) {
        this.url = databaseUrl;
        this.databaseName = databaseName;
        this.collectionName = collectionName;
    }

    async saveState(grid: number[][], step: number): Promise<void> {
        const client = new MongoClient(this.url);
        try {
            await client.connect();
            const db = client.db(this.databaseName);
            const collection = db.collection(this.collectionName);

            // Delete the step if it was previously saved
            collection.deleteMany({step: step});
            collection.insert({
                step: step,
                grid: grid,
            });
        } finally {
            client.close();
        }
    }

    async getState(step: number): Promise<number[][]> {
        let grid: number[][] = [[]];
        const client = new MongoClient(this.url);
        try {
            await client.connect();
            const db = client.db(this.databaseName);
            const collection = db.collection(this.collectionName);

            let result = await collection.findOne({step: step});
            grid = result.grid;
        } finally {
            client.close();
        }
        return grid;
    }

}