import IStoreHighScore from '../game/IStoreHighScore';

let fs = require('fs');

export class StoreTextFileAdapter implements IStoreHighScore {
    fileSystem;
    path: string;

    constructor(dependencies) {
        this.fileSystem = dependencies.filesystem === undefined ? fs : dependencies.filesystem;
        this.path = dependencies.path === undefined ? './files/highscore.txt' : './files/highscore.test.txt';
    }

    store(high_score: number) {
        this.fileSystem.writeFileSync(this.path, high_score.toString());
    }

    read(): number {
        let content: string = this.fileSystem.readFileSync(this.path);
        return parseInt(content);
    }

}