import IStoreHighScore from './IStoreHighScore';
import IPlay, {code, commands} from './IPlay';
import IDisplay from './IDisplay';
import mergeMovement from './gameFunctions/mergeMovement';
import generateRandomCells from './gameFunctions/generateRandomCells';
import getScore from './gameFunctions/getScore';
import ISaveAndLoad from './ISaveAndLoad';

export const loseMessage = 'Tu as perdu !';
export const invalidInputMessage = 'Commande inconnue, veuillez réessayer.';
export const noPreviousStepMessage = 'Vous êtes à la première étape, impossible de revenir en arrière !';

const move_commands = [
    commands.RIGHT,
    commands.LEFT,
    commands.UP,
    commands.DOWN,
];

export class Game implements IPlay {
    outputDisplay: IDisplay;
    highScoreStorage: IStoreHighScore;
    gridStorage: ISaveAndLoad;

    grid: number[][];
    step: number;
    merge: (direction: string) => void;
    generateRandomCells: (n: number) => void;
    getCurrentScore: () => number;

    constructor(dependencies) {
        this.outputDisplay = dependencies.outputDisplay;
        this.highScoreStorage = dependencies.highScoreStorage;
        this.gridStorage = dependencies.gridStorage;

        this.merge = (direction: string) => this.setGrid(mergeMovement(this.getGrid(), direction));
        this.generateRandomCells = (n: number) => this.setGrid(generateRandomCells(this.getGrid(), n));
        this.getCurrentScore = () => getScore(this.getGrid());
    }

    getGrid(): number[][] {
        return this.grid;
    }

    setGrid(g: number[][]) {
        this.grid = g;
    }

    start() {
        this.grid = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
        this.step = 0;
        this.generateRandomCells(2);
    }

    play(command: string): void {
        if (command === commands.START) {
            this.start();
            this.gridStorage.saveState(this.getGrid(), this.step);
            this.outputDisplay.print(code.SUCCESS, this.getGrid(), '');
        } else if (move_commands.includes(command)) {
            this.merge(command);
            this.updateHighScore(this.getCurrentScore());
            try {
                this.generateRandomCells(1);
                this.step += 1;
                this.gridStorage.saveState(this.getGrid(), this.step);
            } catch (e) {
                this.outputDisplay.print(code.FAILURE, [[]], e.message);
            }
            this.outputDisplay.print(code.SUCCESS, this.getGrid(), '');
        } else if (command === commands.GO_BACK) {
            if (this.step === 1) {
                this.outputDisplay.print(code.FAILURE, [[]], noPreviousStepMessage);
            } else {
                try {
                    this.step -= 1;
                    this.gridStorage.getState(this.step).then(grid => {
                        this.setGrid(grid);
                        this.outputDisplay.print(code.SUCCESS, this.getGrid(), '');
                    });
                } catch (e) {
                    this.outputDisplay.print(code.FAILURE, [[]], e.message);
                }
            }
        } else {
            this.outputDisplay.print(code.FAILURE, [[]], invalidInputMessage);
        }
    }

    updateHighScore(score: number) {
        let high_score: number;
        try {
            high_score = this.highScoreStorage.read();
        } catch (e) {
            high_score = 0;
        }
        if (score > high_score) {
            this.highScoreStorage.store(score);
        }
    }

    askForHelp(helpMessage: string): void {
        this.outputDisplay.displayHelp(helpMessage);
    }

    displayMessage(message: string): void {
        this.outputDisplay.displayInfo(message);
    }
}