export default interface IDisplay {
    print(statusCode: number, grid: number[][], failureMessage: string);

    displayGrid(grid: number[][]): void;

    displayFailure(failureMessage: string);

    displayHelp(helpMessage: string);

    displayInfo(infoMessage: string);

    displayScore(score: number): void;
}