export const commands = {
    UP: 'u',
    DOWN: 'd',
    LEFT: 'l',
    RIGHT: 'r',
    START: 's',
    GO_BACK: 'b',
    HELP: 'h',
};

export const code = {
    SUCCESS: 0,
    FAILURE: 1,
};


export default interface IPlay {
    play(command: string): void;

    askForHelp(helpMessage: string): void;

    displayMessage(message: string): void;
}
