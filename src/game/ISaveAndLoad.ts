export default interface ISaveAndLoad {
    saveState(grid: number[][], step: number): Promise<void>;

    getState(step: number): Promise<number[][]>;
}