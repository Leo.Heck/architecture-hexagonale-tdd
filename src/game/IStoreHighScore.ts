export default interface IStoreHighScore {
    store(high_score: number): void;

    read(): number;
}