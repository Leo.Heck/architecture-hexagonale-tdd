import {loseMessage} from '../Game';

function getEmptyCells(grid: number[][]): number[][] {
    let empty_cells = [];
    grid.forEach((line, i) => {
        line.forEach((cell, j) => {
            if (cell === 0) {
                empty_cells.push([i, j]);
            }
        });
    });
    return empty_cells;
}

export default function generateRandomCells(grid: number[][], n: number): number[][] {
    let empty_cells = getEmptyCells(grid);
    if (empty_cells.length >= n) {
        empty_cells.sort(() => .5 - Math.random());
        empty_cells.slice(0, n).forEach(coords => {
            grid[coords[0]][coords[1]] = 2;
        });
        return grid;
    } else {
        throw Error(loseMessage);
    }
}