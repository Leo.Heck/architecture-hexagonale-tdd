export default function getCurrentScore(grid: number[][]) : number {
    let score = 0;
    grid.forEach((ligne) => {
        ligne.forEach((cell) => {
            score = cell > score ? cell : score;
        })
    })
    return score;
}