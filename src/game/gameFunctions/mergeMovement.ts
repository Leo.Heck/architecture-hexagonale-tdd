import {commands} from '../IPlay';

function mergeLeft(grid: number[][]): number[][] {
    return (grid.map(line => {
        let new_line = [0, 0, 0, 0];
        let last_seen = 0;
        let j = 0;
        line.forEach(cell => {
            if (cell > 0) {
                if (last_seen === 0) {
                    last_seen = cell;
                } else if (last_seen === cell) {
                    new_line[j] = cell * 2;
                    j += 1;
                    last_seen = 0;
                } else {
                    new_line[j] = last_seen;
                    last_seen = cell;
                    j += 1;
                }
            }
        });
        if (last_seen > 0) {
            new_line[j] = last_seen;
        }
        return new_line;
    }));
}

function clockwiseRotation(grid: number[][]): number[][] {
    let new_grid = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
    grid.forEach((line, i) => {
        line.forEach((cell, j) => {
            new_grid[j][3 - i] = cell;
        });
    });
    return new_grid;
}

export default function mergeMovement(grid: number[][], direction: string): number[][] {
    if (direction === commands.LEFT) {
        return mergeLeft(grid);
    } else if (direction === commands.DOWN) {
        grid = clockwiseRotation(grid);
        grid = mergeLeft(grid);
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        return grid;
    } else if (direction === commands.UP) {
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        grid = mergeLeft(grid);
        grid = clockwiseRotation(grid);
        return grid;
    } else if (direction === commands.RIGHT) {
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        grid = mergeLeft(grid);
        grid = clockwiseRotation(grid);
        grid = clockwiseRotation(grid);
        return grid;
    }
}