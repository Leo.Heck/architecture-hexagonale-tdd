import {ConsoleReaderAdapter} from './ui/console/ConsoleReaderAdapter';
import {KeyboardReaderAdapter} from './ui/console/KeyboardReaderAdapter';
import {Game} from './game/Game';
import {StoreTextFileAdapter} from './data/StoreTextFileAdapter';
import {ConsoleWriterAdapter} from './ui/console/ConsoleWriterAdapter';
import {GraphicConsoleWriterAdapter} from './ui/console/GraphicConsoleWriterAdapter';
import {MongodbAdapter} from './data/MongodbAdapter';

const DATABASE_PARAMETERS = {
    databaseUrl: 'mongodb://localhost:27017',
    databaseName: '2048db',
    collectionName: 'grids',
};

let storage = new StoreTextFileAdapter({});
let gridStorage = new MongodbAdapter(DATABASE_PARAMETERS);
let outputConsole = new ConsoleWriterAdapter();
// let outputConsole = new GraphicConsoleWriterAdapter();

let game = new Game({
    outputDisplay: outputConsole,
    highScoreStorage: storage,
    gridStorage: gridStorage,
});

let inputConsole = new ConsoleReaderAdapter(game);
// let inputConsole = new KeyboardReaderAdapter(game);

inputConsole.start().then();
