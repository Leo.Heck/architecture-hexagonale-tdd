import IPlay, {commands} from '../../game/IPlay';
import {createInterface, Interface} from 'readline';

const HELP_MESSAGE = 'Commandes : '
    + commands.UP + ' for up, '
    + commands.DOWN + ' for down, '
    + commands.LEFT + ' for left, '
    + commands.RIGHT + ' for right, '
    + commands.GO_BACK + ' to go back, '
    + commands.START + ' for restart, '
    + commands.HELP + ' to display commands';
const END_MESSAGE = 'Goodbye !';

export class ConsoleReaderAdapter {
    game: IPlay;
    consoleInput: Interface;

    constructor(game: IPlay, input = process.stdin, output = process.stdout) {
        this.game = game;
        this.consoleInput = createInterface({
            input: input,
            output: output,
        });
        this.consoleInput.on('close', () => {
            this.game.displayMessage(END_MESSAGE);
            if (process.env.NODE_ENV !== 'test') {
                process.exit(0);
            }
        });

    }

    async start() {
        this.game.play(commands.START);
        this.game.askForHelp(HELP_MESSAGE);

        while (true) {
            await this.playIteration();
        }
    }

    async playIteration() {
        let rep: string = await this.askUserInput();
        if (rep === commands.HELP) this.game.askForHelp(HELP_MESSAGE);
        else this.game.play(rep);
    }

    askUserInput(): Promise<string> {
        return new Promise((resolve) => {
            this.consoleInput.question('',
                (answer) => {
                    resolve(answer);
                },
            );
        });
    }

    close() {
        this.consoleInput.close();
    }


}