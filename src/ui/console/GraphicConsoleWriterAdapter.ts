import IDisplay from '../../game/IDisplay';
import {code} from '../../game/IPlay';

export class GraphicConsoleWriterAdapter implements IDisplay {
    outputConsole: Console;

    constructor(outputConsole = console) {
        this.outputConsole = outputConsole;
    }

    print(statusCode: number, grid: number[][], failureMessage: string) {
        switch (statusCode) {
            case(code.SUCCESS):
                this.displayGrid(grid);
                break;
            case(code.FAILURE):
                this.displayFailure(failureMessage);
                break;
            default:
                this.displayFailure('Erreur interne');
        }
    }

    displayGrid(grid: number[][]): void {
        let displayString: string = '\n';
        grid.forEach(line => {
            line.forEach(value => {
                displayString += new Array(6 - value.toString().length).join(' ') + (value ? value : '.');
            });
            displayString += '\n';
        });
        this.outputConsole.log(displayString);
    }

    displayFailure(failureMessage: string) {
        this.outputConsole.error(failureMessage);
    }

    displayScore(score: number): void {
    }

    displayHelp(helpMessage: string) {
        this.outputConsole.log(helpMessage + '\n');
    }

    displayInfo(infoMessage: string) {
        this.outputConsole.log('\n' + infoMessage + '\n');
    }

}
