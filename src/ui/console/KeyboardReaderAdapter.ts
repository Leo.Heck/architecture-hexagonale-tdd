import IPlay, {commands} from '../../game/IPlay';
import readline from 'readline';

const mapping = {
    'up': 'u',
    'down': 'd',
    'left': 'l',
    'right': 'r',
    'backspace': 'b',
    'r': 's',
    'h': 'h',
};

const HELP_MESSAGE = 'Press arrow keys to play, backspace to go back, \'r\' to restart and \'h\' to display help';
const END_MESSAGE = 'End of game !';

export class KeyboardReaderAdapter {
    game: IPlay;

    constructor(game: IPlay, input = process.stdin, output = process.stdout) {
        this.game = game;
        readline.emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);

        process.stdin.on('keypress', (key, data) => {
            if (data.ctrl && data.name === 'c') {
                this.game.displayMessage(END_MESSAGE);
                if (process.env.NODE_ENV !== 'test') {
                    process.exit(0);
                }
            }
        });

    }

    async start() {
        this.game.play(commands.START);
        this.game.askForHelp(HELP_MESSAGE);

        while (true) {
            await this.playIteration();
        }
    }

    async playIteration() {
        let rep: string = await this.askUserInput();
        if (rep === commands.HELP) this.game.askForHelp(HELP_MESSAGE);
        else this.game.play(rep);
    }

    askUserInput(): Promise<string> {
        return new Promise((resolve) => {
            process.stdin.once('keypress', (key, data) => {
                resolve(mapping[data.name]);
            });
        });
    }

    close() {
    }
}
