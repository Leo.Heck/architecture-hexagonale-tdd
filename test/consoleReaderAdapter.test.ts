import {ConsoleReaderAdapter} from '../src/ui/console/ConsoleReaderAdapter';
import {KeyboardReaderAdapter} from '../src/ui/console/KeyboardReaderAdapter';
import IPlay, {commands} from '../src/game/IPlay';
import {expect, sinon} from './test-helper';
import {Stream} from 'stream';


let stdin = require('mock-stdin').stdin();


const g = new Stream();

class GameStub implements IPlay {
    play = sinon.stub();
    askForHelp = sinon.stub();
    displayMessage = sinon.stub();
}

describe('Call to play with console', () => {
    it('should call play with the START commands when started', () => {
        let gameStub = new GameStub();
        let consoleAdapter = new ConsoleReaderAdapter(gameStub);

        consoleAdapter.start();
        consoleAdapter.close();
        expect(gameStub.play).to.have.been.calledOnce;
        expect(gameStub.play).to.have.been.calledWith(commands.START);


    });

    it('should call play with the input argument', () => {
        let gameStub = new GameStub();

        let consoleAdapter = new ConsoleReaderAdapter(gameStub, stdin);
        const input = 'input';

        consoleAdapter.playIteration().then(() => consoleAdapter.close());
        stdin.send(input + '\n');

        setTimeout(() => {
            expect(gameStub.play).to.have.been.calledOnce;
            expect(gameStub.play).to.have.been.calledWith(input);
        }, 100);


    });
});

describe('Call to play with keyboard', () => {
    it('should call play with the START commands when started', () => {
        let gameStub = new GameStub();
        let consoleAdapter = new KeyboardReaderAdapter(gameStub);

        consoleAdapter.start();
        consoleAdapter.close();
        expect(gameStub.play).to.have.been.calledOnce;
        expect(gameStub.play).to.have.been.calledWith(commands.START);


    });

    it('should call play with the up key code', () => {
        let gameStub = new GameStub();

        let consoleAdapter = new KeyboardReaderAdapter(gameStub, stdin);
        const input = '\u001b[A';
        const cancelInput = '\u0003';
        const key = 'u';

        consoleAdapter.playIteration().then(() => {
            stdin.send(cancelInput);
            expect(gameStub.play).to.have.been.calledOnce;
            expect(gameStub.play).to.have.been.calledWith(key);
        });
        stdin.send(input);
    });
});
