import {expect, sinon} from './test-helper';
import {ConsoleWriterAdapter} from '../src/ui/console/ConsoleWriterAdapter';
import {GraphicConsoleWriterAdapter} from '../src/ui/console/GraphicConsoleWriterAdapter';
import {code} from '../src/game/IPlay';

let consoleLogStub;
let consoleErrorStub;
let sandbox;

beforeEach(() => {
    sandbox = sinon.createSandbox();
    consoleLogStub = sandbox.stub(console, 'log');
    consoleErrorStub = sandbox.stub(console, 'error');
});

afterEach(() => {
    sandbox.restore();
});

describe('Console writer', () => {
    it('should correctly display a game grid', () => {
        let outputConsole = new ConsoleWriterAdapter();
        let grid = [
            [0, 2, 4, 2],
            [16, 8, 0, 0],
            [2, 2, 2, 4],
            [0, 32, 4, 2],
        ];

        outputConsole.print(code.SUCCESS, grid, '');
        let expectedString = '\n0 2 4 2 \n16 8 0 0 \n2 2 2 4 \n0 32 4 2 \n';

        expect(consoleLogStub).to.have.been.calledOnce;
        expect(consoleLogStub).to.have.been.calledWith(expectedString);
    });

    it('should display the failure message when called with failure code', () => {
        let outputConsole = new ConsoleWriterAdapter();
        let failureMessage = 'Failure';

        outputConsole.print(code.FAILURE, [[]], failureMessage);

        expect(consoleErrorStub).to.have.been.calledOnce;
        expect(consoleErrorStub).to.have.been.calledWith(failureMessage);
    });
});

describe('Graphic Console writer', () => {
    it('should correctly display a game grid', () => {
        let outputConsole = new GraphicConsoleWriterAdapter();
        let grid = [
            [0, 2, 4, 2],
            [16, 8, 0, 0],
            [2, 2, 2, 4],
            [0, 32, 4, 2],
        ];

        outputConsole.print(code.SUCCESS, grid, '');
        let expectedString = "\n    .    2    4    2\n   16    8    .    .\n    2    2    2    4\n    .   32    4    2\n";

        expect(consoleLogStub).to.have.been.calledOnce;
        expect(consoleLogStub).to.have.been.calledWith(expectedString);
    });

    it('should display the failure message when called with failure code', () => {
        let outputConsole = new GraphicConsoleWriterAdapter();
        let failureMessage = 'Failure';

        outputConsole.print(code.FAILURE, [[]], failureMessage);

        expect(consoleErrorStub).to.have.been.calledOnce;
        expect(consoleErrorStub).to.have.been.calledWith(failureMessage);
    });
});
