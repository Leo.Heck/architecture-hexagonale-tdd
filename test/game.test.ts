import {Game, loseMessage} from '../src/game/Game';
import {commands} from '../src/game/IPlay';
import {expect, sinon} from './test-helper';
import IStoreHighScore from '../src/game/IStoreHighScore';


class StoreStub implements IStoreHighScore {
    store = sinon.stub();
    read = sinon.stub().returns(64);
}

let dependencies = {};


describe('Initialisation', () => {
    it('should start with a 4*4 grid with two cells at 2 and the others at 0', () => {
        let game = new Game(dependencies);
        game.start();
        let grid = game.getGrid();

        expect(grid.length).equal(4);
        expect(grid[0].length).equal(4);
        let count2 = 0;
        let count0 = 0;
        grid.forEach(value => {
            value.forEach(value1 => {
                if (value1 === 0) {
                    count0 += 1;
                } else if (value1 === 2) {
                    count2 += 1;
                }
            });
        });
        expect(count2).equal(2);
        expect(count0).equal(14);
    });
});

describe('Movements', () => {
    let game = new Game(dependencies);
    game.start();

    it('should merge with left', () => {
        game.setGrid([
            [8, 4, 4, 0],
            [0, 2, 0, 2],
            [0, 4, 2, 0],
            [4, 4, 4, 4],
        ]);
        game.merge(commands.LEFT);

        expect(game.getGrid()).to.eql([
            [8, 8, 0, 0],
            [4, 0, 0, 0],
            [4, 2, 0, 0],
            [8, 8, 0, 0],
        ]);

    });
    it('should merge with right', () => {
        game.setGrid([
            [8, 4, 4, 0],
            [0, 2, 0, 2],
            [0, 4, 2, 0],
            [4, 4, 4, 4],
        ]);
        game.merge(commands.RIGHT);

        expect(game.getGrid()).to.eql([
            [0, 0, 8, 8],
            [0, 0, 0, 4],
            [0, 0, 4, 2],
            [0, 0, 8, 8],
        ]);
    });
    it('should merge with down', () => {
        game.setGrid([
            [8, 4, 4, 0],
            [0, 2, 0, 2],
            [0, 4, 2, 2],
            [4, 4, 4, 4],
        ]);
        game.merge(commands.DOWN);

        expect(game.getGrid()).to.eql([
            [0, 0, 0, 0],
            [0, 4, 4, 0],
            [8, 2, 2, 4],
            [4, 8, 4, 4],
        ]);
    });
    it('should merge with up', () => {
        game.setGrid([
            [8, 4, 4, 0],
            [0, 2, 0, 2],
            [0, 4, 2, 2],
            [4, 4, 4, 4],
        ]);
        game.merge(commands.UP);

        expect(game.getGrid()).to.eql([
            [8, 4, 4, 4],
            [4, 2, 2, 4],
            [0, 8, 4, 0],
            [0, 0, 0, 0],
        ]);
    });
});

describe('Cell generator', () => {
    let game = new Game(dependencies);
    game.start();
    it('should add a 2 in an empty cell', () => {
        game.setGrid([
            [0, 4, 4, 4],
            [0, 4, 4, 4],
            [4, 4, 4, 4],
            [4, 4, 4, 4],
        ]);
        game.generateRandomCells(1);
        expect([
            [
                [2, 4, 4, 4],
                [0, 4, 4, 4],
                [4, 4, 4, 4],
                [4, 4, 4, 4],
            ],
            [
                [0, 4, 4, 4],
                [2, 4, 4, 4],
                [4, 4, 4, 4],
                [4, 4, 4, 4],
            ],
        ]).to.deep.include(game.getGrid());
    });
    it('should throw an error if there is not enough space', () => {
        game.setGrid([
            [0, 4, 4, 4],
            [0, 4, 4, 4],
            [4, 4, 4, 4],
            [4, 4, 4, 4],
        ]);
        let err = null;
        try {
            game.generateRandomCells(3);
        } catch (error) {
            err = error;
        }

        expect(err).to.be.an('error');
        expect(err.message).to.equal(loseMessage);
    });
});

describe('Highscore management', () => {
    it('should calculate the current score', () => {
        let game = new Game(dependencies);
        game.setGrid([
            [8, 4, 4, 0],
            [0, 2, 0, 2],
            [0, 4, 2, 2],
            [4, 4, 4, 4],
        ]);
        let n1: number = game.getCurrentScore();
        game.setGrid([
            [8, 4, 4, 32],
            [0, 2, 0, 2],
            [0, 4, 2, 2],
            [4, 4, 4, 4],
        ]);
        let n2: number = game.getCurrentScore();

        expect(n1).equal(8);
        expect(n2).equal(32);
    });

    it('should update the score only if the highscore is smaller - case score smaller', () => {
        let storestub = new StoreStub();
        let game = new Game({highScoreStorage: storestub}); //current high score = 64

        game.updateHighScore(32);

        expect(storestub.read).to.have.been.calledOnce;
        expect(storestub.store).to.have.not.been.called;
    });

    it('should update the score only if the highscore is smaller - case score higher', () => {
        let storestub = new StoreStub();
        let game = new Game({highScoreStorage: storestub}); //current high score = 64

        game.updateHighScore(128);

        expect(storestub.read).to.have.been.calledOnce;
        expect(storestub.store).to.have.been.calledOnce;
        expect(storestub.store).to.have.been.calledWith(128);

    });

});