import {Game} from '../src/game/Game';
import {StoreTextFileAdapter} from '../src/data/StoreTextFileAdapter';
import {GraphicConsoleWriterAdapter} from '../src/ui/console/GraphicConsoleWriterAdapter';
import {expect, sinon} from './test-helper';
let fs = require('fs');

const path_highscore_test = './files/highscore.test.txt';

let storage;
let outputConsoleStub;
let game;


describe("Integration file storage", () => {

    before(function() {
        storage = new StoreTextFileAdapter({path: path_highscore_test});
        outputConsoleStub = sinon.createStubInstance(GraphicConsoleWriterAdapter);
        game = new Game({outputDisplay: outputConsoleStub, highScoreStorage: storage});

    });

    after(function() {
        fs.unlinkSync(path_highscore_test);
    });

    it('should update the score only if the highscore is smaller - case first time', () => {

        game.updateHighScore(32);
        let hs:number = storage.read();
        expect(hs).to.equal(32)
    });

    it('should update the score only if the highscore is smaller - case score higher', () => {

        game.updateHighScore(128);
        let hs:number = storage.read();
        expect(hs).to.equal(128)

    });

    it('should update the score only if the highscore is smaller - case score smaller', () => {

        game.updateHighScore(64);
        let hs:number = storage.read();
        expect(hs).to.equal(128)

    });

})
