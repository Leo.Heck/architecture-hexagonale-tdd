import {StoreTextFileAdapter} from "../src/data/StoreTextFileAdapter";
import {expect, sinon} from "./test-helper"

class FileSystemStub {
    writeFileSync = sinon.stub()
    readFileSync = sinon.stub().returns('1000')
}


describe('File Writer',() => {
    it('should create and fill the file "files/high_score.test.txt" with the number given to store method ', () => {
        let fsStub = new FileSystemStub()
        let storage = new StoreTextFileAdapter({filesystem: fsStub});

        storage.store(1000);

        expect(fsStub.writeFileSync).to.have.been.calledOnce;
        expect(fsStub.writeFileSync).to.have.been.calledWith('./files/highscore.txt', '1000');
    })

    it('should read this file when read method is called', () => {
        let fsStub = new FileSystemStub()
        let storage = new StoreTextFileAdapter({filesystem: fsStub});

        const n = storage.read();

        expect(fsStub.readFileSync).to.have.been.calledOnce;
        expect(fsStub.readFileSync).to.have.been.calledWith('./files/highscore.txt');
        expect(n).to.equal(1000);
    })

})
